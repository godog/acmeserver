acmeserver
====

Runs a daemon to manage a set of SSL certificates using the ACME protocol.

There are many similar tools, why another one? Well we need a few
unique features:

* custom output code for certificates and private keys, so we can
  write them to [replds](https://git.autistici.org/ai3/replds) and
  have them replicated to all front-ends;
* support for our DNS setup for *dns-01* challenges, by sending RFC
  2136 updates to all DNS servers in parallel.

For the rest it's a fairly common ACME automation tool, it supports
the *http-01* and *dns-01* challenges (no *tls-sni-01* because the
tool is meant to be run behind a HTTPS proxy so it can't directly
control the serving certificates).

Since this is a particularly critical piece of software, a few extra
cautions are necessary in its development:

* do not implement any ACME-specific code but use a well-maintained
  library instead
  (like [golang.org/x/crypto/acme](https://golang.org/x/crypto/acme))
* try to be robust against ACME high-level protocol changes by keeping
  this tool replaceable with *certbot* and a bunch of shell
  scripts. In particular we can do this by:
  * keeping a directory structure for the output that's compatible
    with certbot
  * having a way to independently push content to replds (which we do,
    by way of the *replds* command itself)
    
  So the advantage of *acmeserver* becomes just the integration
  between the various components in a single package / binary (and
  monitoring, etc).
